describe('Login', () => {
  describe('When visit login url', () => {
    const valid_name = 'email_1@gmail.com';
    const valid_password = 'password1';

    beforeEach(() => {
      cy.visit('/login');
    });

    it('contains h1 login', () => {
      cy.contains('h1', 'Login');
    });

    it('contains inputs', () => {
      cy.get('#email');
      cy.get("#password");
    });

    describe('when have bad email', () => {
      it('shoud display email error', () => {
        cy.get('#email').type('c2');
        cy.get('#password').type(valid_password);
        cy.contains('#errors', 'email');
      });
    });

    describe('when have a bad password', () => {
      it('shoud display password error', () => {
        cy.get('#email').type(valid_name);
        cy.get('#password').type('11');
        cy.contains('#errors', 'password');
      });
    });

    describe('when have valid data', () => {
      it('shoud go to UserForm', () => {
        cy.get('#email').type(valid_name);
        cy.get('#password').type(valid_password);
        cy.get('button').click();
        cy.url().should('eq', 'http://localhost:8080/userForm');
      });
    });
  });
});
