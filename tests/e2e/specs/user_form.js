describe('UserForm', () => {
  describe('When visit userform url', () => {
    const valid_email = 'email_1@gmail.com';
    const encoded_email = 'email_1%40gmail.com';

    beforeEach(() => {
      cy.visit('/userForm');
    });

    it('contains inputs', () => {
      cy.get('#email');
      cy.get("#age");
    });

    it('should display h1 UserForm', () => {
      cy.contains('h1', 'UserForm');
    });

    describe('when have a bad age', () => {
      it('should display an age error message', () => {
        cy.get('#age').clear();
        cy.get('#age').type('17');
        cy.contains('#errors', 'Age');
      });
    });

    describe('when have a bad OS', () => {
      it('should display an OS error message', () => {
        cy.contains('#errors', 'OS');
      });
    });

    describe('when have good data', () => {
      it('should display an success message', () => {
        cy.get('#age').clear();
        cy.get('#age').type('18');
        cy.get('#osinput').select('Linux');
        cy.contains('#success', 'valid data');
      });
    });
  });
});
