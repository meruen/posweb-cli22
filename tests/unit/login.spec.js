import { shallowMount } from '@vue/test-utils';
import Login from '@/components/Login.vue';

describe('Login', () => {
  it('is a valid component ', () => {
    const wrapper = shallowMount(Login);
    expect(wrapper.exists()).toBe(true);
  });

  describe('when email have a bad format', () => {
    it('should display an message containing a email error', () => {
      const wrapper = shallowMount(Login, {
        data: () => ({ email: 'bademail', password: 'pwd' }),
      });
      expect(wrapper.find('#errors').html()).toContain('Bad email');
    });
  });

  describe('when email have a good format', () => {
    it('should display no email error', () => {
      const wrapper = shallowMount(Login, {
        data: () => ({ email: 'abc@abc.com', password: 'pwd' }),
      });
      expect(wrapper.find('#errors').html()).not.toContain('Bad email');
    });
  });

  describe('when password is smaller than 6 characters', () => {
    it('should display a message with a password error', () => {
      const wrapper = shallowMount(Login, {
        data: () => ({ email: 'abc@abc.com', password: 'pwd' }),
      });
      expect(wrapper.find('#errors').html()).toContain('The password must');
    });
  });

  describe('when password have 6 or more characters', () => {
    it('should display no password error', () => {
      const wrapper = shallowMount(Login, {
        data: () => ({ email: 'abc@abc.com', password: '123123' }),
      });
      expect(wrapper.find('#errors').html()).not.toContain('The password must');
    });
  });

  describe('when login is clicked with bad data', () => {
    it('should display the access deny message', () => {
      const wrapper = shallowMount(Login, {
        data: () => ({ email: 'abc@abc.com', password: '123123' }),
      });
      wrapper.vm.doLogin();
      expect(wrapper.find('#abort')).not.toBe(null);
    });
  });
});
