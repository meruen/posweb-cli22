import { shallowMount } from '@vue/test-utils';
import UserForm from '@/components/UserForm.vue';

describe('UserForm', () => {
  it('is a valid component ', () => {
    const wrapper = shallowMount(UserForm);
    expect(wrapper.exists()).toBe(true);
  });

  describe('when age is lower than 18', () => {
    it('should display an age error', () => {
      const wrapper = shallowMount(UserForm, {
        data: () => ({ age: '17' }),
      });
      expect(wrapper.find('#errors').html()).toContain('Age');
    });
  });

  describe('when age is higher than 99', () => {
    it('should display an age error', () => {
      const wrapper = shallowMount(UserForm, {
        data: () => ({ age: '100' }),
      });
      expect(wrapper.find('#errors').html()).toContain('Age');
    });
  });

  describe('when age is between 18 and 99', () => {
    it('should not display an age error', () => {
      const wrapper = shallowMount(UserForm, {
        data: () => ({ age: '18' }),
      });
      expect(wrapper.find('#errors').html()).not.toContain('Age');
    });
  });

  describe('when OS is not selected', () => {
    it('should display an OS error message', () => {
      const wrapper = shallowMount(UserForm, {
        data: () => ({ age: '18', os: '' }),
      });
      expect(wrapper.find('#errors').html()).toContain('OS');
    });
  });

  describe('when OS is selected', () => {
    it('should not display an OS error message', () => {
      const wrapper = shallowMount(UserForm, {
        data: () => ({ age: '18', os: 'Linux' }),
      });
      expect(wrapper.find('#errors').html()).not.toContain('OS');
    });
  });

  describe('when we have valid data', () => {
    it('should display a success message', () => {
      const wrapper = shallowMount(UserForm, {
        data: () => ({ age: '18', os: 'Linux' }),
      });
      expect(wrapper.find('#success').html()).toContain('valid data');
    });
  });
});
